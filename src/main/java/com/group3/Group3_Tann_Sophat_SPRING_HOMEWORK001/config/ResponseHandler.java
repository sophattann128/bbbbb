package com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@Component
public class ResponseHandler<T> {
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T customer;
    private String status;
    private Date time;

    public ResponseHandler(String message, T customer, String status, Date time) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.time = time;
    }

    public ResponseHandler(String message, String status, Date time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public ResponseEntity<ResponseHandler<T>> generateResponse(String message, T customer, String status){
        return ResponseEntity.ok(new ResponseHandler<>(message,customer,status,new Date()) );
    }
    public ResponseEntity<ResponseHandler<T>> generateResponse(String message, String status){

        return ResponseEntity.ok(new ResponseHandler<>(message,status, new Date()) );

    }
}
