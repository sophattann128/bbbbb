package com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.controller;

import com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.config.ResponseHandler;
import com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.entities.Customer;
import com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.entities.CustomerRequest;
import com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ResponseHandler<Customer> customerResponseHandler;
    List<Customer> customerList = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<Customer>> showCustomerList() {
        return ResponseEntity.ok(customerList);

    }

    @GetMapping("/{customerId}")
    public ResponseEntity<ResponseHandler<Customer>> getCustomerById(@PathVariable("customerId") long id) {
        try {
            Customer customer = customerService.findById(id, customerList);
            if (customer != null) {
                return customerResponseHandler.generateResponse("Successfully found data", customer, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return customerResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS.name());
        }
    }

    @GetMapping("/search")
    public ResponseEntity<ResponseHandler<Customer>> searchByName(@RequestParam String name) {
        try {
            Customer customer = customerService.findByName(name, customerList);
            if (customer != null) {
                return customerResponseHandler.generateResponse("Successfully found data", customer, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return customerResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS.name());
        }
    }

    @PostMapping
    public ResponseEntity<ResponseHandler<Customer>> addCustomer(@RequestBody CustomerRequest customer) {
        try {
            Customer addCustomer = customerService.addNewCustomer(customer, customerList);
            return customerResponseHandler.generateResponse("Successfully created data", addCustomer, HttpStatus.OK.name());
        } catch (Exception e) {
            return customerResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS.name());
        }
    }

    @PutMapping("/{customerId}")
    public ResponseEntity<ResponseHandler<Customer>> updateById(@PathVariable("customerId") long id, @RequestBody CustomerRequest customer) {
        try {
            Customer findCustomer = customerService.findById(id, customerList);
            Customer updateCustomer = customerService.updateById(id, customer, customerList);
            if (findCustomer != null) {
                return customerResponseHandler.generateResponse("Successfully updated data", updateCustomer, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return customerResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS.name());
        }
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<ResponseHandler<Customer>> delete(@PathVariable("customerId") long id) {
        try {
            Customer customerToDelete = customerService.findById(id, customerList);
            if (customerToDelete != null) {
                customerService.deleteById(id, customerList);
                return customerResponseHandler.generateResponse("Successfully deleted data", HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return customerResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS.name());
        }
    }


}
