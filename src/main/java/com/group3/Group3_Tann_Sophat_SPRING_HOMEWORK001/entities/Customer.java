package com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Customer {
    private long id;
    private String name;
    private String gender;
    private Integer age;
    private String address;

}
