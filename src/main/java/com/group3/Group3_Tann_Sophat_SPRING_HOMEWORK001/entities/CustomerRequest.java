package com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.entities;

import lombok.Data;

@Data
public class CustomerRequest {
    private String name;
    private String gender;
    private Integer age;
    private String address;
}
