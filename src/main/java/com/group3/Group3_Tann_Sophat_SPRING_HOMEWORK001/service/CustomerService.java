package com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.service;

import com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.entities.Customer;
import com.group3.Group3_Tann_Sophat_SPRING_HOMEWORK001.entities.CustomerRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {
    static int id = 1;
    public List<Customer> findAllCustomer(List<Customer> customerList) {
        return customerList;
    }
    public Customer addNewCustomer(CustomerRequest customerRequest, List<Customer> customers) {
        Customer customer = new Customer(id++, customerRequest.getName(), customerRequest.getGender(), customerRequest.getAge(), customerRequest.getAddress());
        customers.add(customer);
        return customer;
    }

    public Customer updateById(long id, CustomerRequest customer, List<Customer> customers) {
        Customer cus = null;
        for (Customer person : customers) {
            if (person.getId() == id) {
                person.setName(customer.getName());
                person.setAge(customer.getAge());
                person.setGender(customer.getGender());
                person.setAddress(customer.getAddress());
                cus = new Customer(person.getId(), customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress());
            }
        }

        return cus;
    }

    public Customer findByName(String keyword, List<Customer> customers) {
        Customer customer = null;
        for (Customer cus : customers) {
            if (Objects.equals(cus.getName(), keyword)) {
                customer = cus;
            }
        }
        return customer;
    }

    public Customer findById(long id, List<Customer> customers) {
        Customer customer = null;
        for (Customer cus : customers) {
            if (Objects.equals(cus.getId(), id)) {
                customer = cus;
            }
        }
        return customer;
    }

    public void deleteById(long id, List<Customer> customers) {
        customers.removeIf(item -> item.getId() == id);
    }
}
